//-----------------------------------------------------------------------------
// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2010-2013 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//
//      SVN Information
//
//      Checked In          : $Date: 2013-04-10 15:27:13 +0100 (Wed, 10 Apr 2013) $
//
//      Revision            : $Revision: 243506 $
//
//      Release Information : Cortex-M System Design Kit-r1p0-00rel0
//
// ----------------------------------------------------------------------------
//  Abstract : FPGA BlockRam/OnChip SRAM
// ----------------------------------------------------------------------------
// The read operation is pipelined. Write operation is not pipelined.
`include "../../system/fpga_options_defs.v"
module cmsdk_fpga_rom #(
// --------------------------------------------------------------------------
// Parameters
// --------------------------------------------------------------------------
  parameter AW = 14
 )
 (
  // Inputs
  input  wire          CLK,
  input  wire [AW-1:0] ADDR,
  input  wire [31:0]   WDATA,
  input  wire [3:0]    WREN,
  input  wire          CS,

  // Outputs
  output wire [31:0]   RDATA
  );

  wire  [31:0] dia;
  wire  [3:0]  wea;

  wire  [31:0] doa;

  assign dia = WDATA [31:0];
  assign wea = WREN [3:0];

  assign doa = RDATA [31:0];

  fpga_sram u_fpga_rom(
     // inputs
    .dia         (dia),
    .addra       (ADDR),
    .cea         (CS),
    .clka        (CLK),
    .wea         (wea),

     // outputs
    .doa         (doa)
    );

endmodule
