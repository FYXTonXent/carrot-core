//------------------------------------------------------------------------------
// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited or its affiliates.
//
//            (C) COPYRIGHT 2010-2015 ARM Limited or its affiliates.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited or its affiliates.
//
//  Version and Release Control Information:
//
//  File Revision       : $Revision: 368442 $
//  File Date           : $Date: 2017-07-25 15:07:59 +0100 (Tue, 25 Jul 2017) $
//
//  Release Information : Cortex-M0 DesignStart-r2p0-00rel0
//
//------------------------------------------------------------------------------
// Verilog-2001 (IEEE Std 1364-2001)
//------------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
// Abstract : Pin multiplexing control for example Cortex-M0/Cortex-M0+
//            microcontroller
//-----------------------------------------------------------------------------
//
module cmsdk_mcu_pin_mux (
    //-------------------------------------------
    // I/O ports
    //-------------------------------------------
    output wire  [15:0]     p0_in,
    input  wire  [15:0]     p0_out,
    input  wire  [15:0]     p0_outen,
    input  wire  [15:0]     p0_altfunc,

    output wire  [15:0]     p1_in,
    input  wire  [15:0]     p1_out,
    input  wire  [15:0]     p1_outen,
    input  wire  [15:0]     p1_altfunc,

    // Processor debug interface
    output wire             i_trst_n,
    output wire             i_swditms,
    output wire             i_swclktck,
    output wire             i_tdi,
    input  wire             i_tdo,
    input  wire             i_tdoen_n,
    input  wire             i_swdo,
    input  wire             i_swdoen,

    // IO pads
    inout  wire   [15:0]    P0,
    inout  wire   [15:0]    P1,

    input  wire             nTRST, // Not needed if serial-wire debug is used
    input  wire             TDI,   // Not needed if serial-wire debug is used
    inout  wire             SWDIOTMS,
    input  wire             SWCLKTCK,
    output wire             TDO
  );   // Not needed if serial-wire debug is used

  //-------------------------------------------
  // Internal wires
  //-------------------------------------------
  wire      [15:0]     p0_out_mux;
  wire      [15:0]     p1_out_mux;

  wire      [15:0]     p0_out_en_mux;
  wire      [15:0]     p1_out_en_mux;
  
  wire                 uart0_rxd;
  wire                 uart0_txd;
  wire                 uart0_txen;
  wire                 uart1_rxd;
  wire                 uart1_txd;
  wire                 uart1_txen;
  wire                 uart2_rxd;
  wire                 uart2_txd;
  wire                 uart2_txen;
  wire                 uart3_rxd;
  wire                 uart3_txd;
  wire                 uart3_txen;

  wire                 ssp0_clk_in;           // SPI clock (Slave)
  wire                 ssp0_clk_out;          // SPI clock (Master)
  wire                 ssp0_clk_out_en_n;     // SPI clock output enable (active low)
  wire                 ssp0_data_out;         // SPI data out
  wire                 ssp0_data_out_en_n;    // SPI data output enable (active low)
  wire                 ssp0_data_in;          // SPI data in
  wire                 ssp0_fss_in;           // SPI device select (Slave) 
  wire                 ssp0_fss_out;          // SPI device select (Master)  

  wire                 ssp1_clk_in;           // SPI clock (Slave)
  wire                 ssp1_clk_out;          // SPI clock (Master)
  wire                 ssp1_clk_out_en_n;     // SPI clock output enable (active low)
  wire                 ssp1_data_out;         // SPI data out
  wire                 ssp1_data_out_en_n;    // SPI data output enable (active low)
  wire                 ssp1_data_in;          // SPI data in
  wire                 ssp1_fss_in;           // SPI device select (Slave) 
  wire                 ssp1_fss_out;          // SPI device select (Master) 

  wire                 i2c0_scl;              // I2C clock
  wire                 i2c0_sda;              // I2C data in
  wire                 i2c0_scl_en_n;         // I2C clock enable
  wire                 i2c0_sda_en_n;         // I2C data enable

  wire                 i2c1_scl;              // I2C clock
  wire                 i2c1_sda;              // I2C data in
  wire                 i2c1_scl_en_n;         // I2C clock enable
  wire                 i2c1_sda_en_n;         // I2C data enable 

  wire                 timer0_extin;
  wire                 timer1_extin;

  // internal peripheral signals
  cmsdk_mcu_system
    u_cmsdk_mcu_system ( 
    // UART
    .uart0_rxd        (uart0_rxd),
    .uart0_txd        (uart0_txd),
    .uart0_txen       (uart0_txen),
    .uart1_rxd        (uart1_rxd),
    .uart1_txd        (uart1_txd),
    .uart1_txen       (uart1_txen),
    .uart2_rxd        (uart2_rxd),
    .uart2_txd        (uart2_txd),
    .uart2_txen       (uart2_txen),
    .uart3_rxd        (uart3_rxd),
    .uart3_txd        (uart3_txd),
    .uart3_txen       (uart3_txen),
    
    // SSP
    .ssp0_data_in       (ssp0_data_in),
    .ssp0_data_out      (ssp0_data_out),
    .ssp0_data_out_en_n (ssp0_data_out_en_n),
    .ssp0_clk_in        (ssp0_clk_in),             // This used as SPI master only. No SPI clock in
    .ssp0_clk_out       (ssp0_clk_out),
    .ssp0_clk_out_en_n  (ssp0_clk_out_en_n),
    .ssp0_fss_in        (ssp0_fss_in),             // Frame select in. This used as SPI master only
    .ssp0_fss_out       (ssp0_fss_out),
    
    .ssp1_data_in       (ssp1_data_in),
    .ssp1_data_out      (ssp1_data_out),
    .ssp1_data_out_en_n (ssp1_data_out_en_n),
    .ssp1_clk_in        (ssp1_clk_in),             // This used as SPI master only. No SPI clock in
    .ssp1_clk_out       (ssp1_clk_out),
    .ssp1_clk_out_en_n  (ssp1_clk_out_en_n),
    .ssp1_fss_in        (ssp1_fss_in),             // Frame select in. This used as SPI master only
    .ssp1_fss_out       (ssp1_fss_out),

    // I2C
    .i2c0_scl         (i2c0_scl),
    .i2c0_sda         (i2c0_sda),
    .i2c0_scl_en_n    (i2c0_scl_en_n),
    .i2c0_sda_en_n    (i2c0_sda_en_n),
    
    .i2c1_scl         (i2c1_scl),
    .i2c1_sda         (i2c1_sda),
    .i2c1_scl_en_n    (i2c1_scl_en_n),
    .i2c1_sda_en_n    (i2c1_sda_en_n),

    // Timer
    .timer0_extin     (timer0_extin),
    .timer1_extin     (timer1_extin)
  );
  
  //-------------------------------------------
  // Beginning of main code
  //-------------------------------------------
  // inputs
    

    
  //bufif0 ( ssp0_fss_in, p0_in[0], p0_out_en_mux[0]);
  bufif0 ( ssp0_clk_in, p0_in[1], p0_out_en_mux[1]);
  bufif0 (ssp0_data_in, p0_in[3], p0_out_en_mux[3]);
  bufif0 ( ssp1_fss_in, p0_in[4], p0_out_en_mux[4]);
  bufif0 ( ssp1_clk_in, p0_in[5], p0_out_en_mux[5]);
  bufif0 (ssp1_data_in, p0_in[7], p0_out_en_mux[7]); 
  
  bufif0 (   uart0_rxd, p1_in[0],  p1_out_en_mux[0]);
  bufif0 (   uart1_rxd, p1_in[2],  p1_out_en_mux[2]);
  bufif0 (   uart2_rxd, p1_in[4],  p1_out_en_mux[4]);
  bufif0 (   uart3_rxd, p1_in[6],  p1_out_en_mux[6]);
  bufif0 (timer0_extin, p1_in[8],  p1_out_en_mux[8]);
  bufif0 (timer1_extin, p1_in[9],  p1_out_en_mux[9]);
  
  //reg temp;
  //bufif0 (    i2c0_scl, p1_in[10],  p1_out_en_mux[10]);
  //bufif0 (    i2c0_sda, p1_in[11],  p1_out_en_mux[11]);
  //bufif0 (    i2c1_scl, p1_in[12],  p1_out_en_mux[12]);
  //bufif0 (    i2c1_sda, p1_in[13],  p1_out_en_mux[13]);
  
  //assign    p1_in[10] = (~p0_out_en_mux[10] && p0_altfunc[10]) ? i2c0_scl : P1[10];
  
  //assign    i2c0_scl = temp ? p1_in[10] : 1'bz;
  
  // Output function mux
  assign    p0_out_mux[0] = (p0_altfunc[0]) ? ssp0_fss_out : p0_out[0];
  assign    p0_out_mux[1] = (p0_altfunc[1]) ? ssp0_clk_out : p0_out[1];
  assign    p0_out_mux[2] = (p0_altfunc[2]) ? ssp0_data_out : p0_out[2];
  assign    p0_out_mux[3] =                                  p0_out[3];
  assign    p0_out_mux[4] = (p0_altfunc[4]) ? ssp1_fss_out : p0_out[4];
  assign    p0_out_mux[5] = (p0_altfunc[5]) ? ssp1_clk_out : p0_out[5];
  assign    p0_out_mux[6] = (p0_altfunc[6]) ? ssp1_data_out : p0_out[6];
  assign    p0_out_mux[7] =                                  p0_out[7];
  assign    p0_out_mux[15:8] = p0_out[15:8];
  
  assign    p1_out_mux[0] =                               p1_out[0];
  assign    p1_out_mux[1] = (p1_altfunc[1]) ? uart0_txd : p1_out[1];
  assign    p1_out_mux[2] =                               p1_out[2];
  assign    p1_out_mux[3] = (p1_altfunc[3]) ? uart1_txd : p1_out[3];
  assign    p1_out_mux[4] =                               p1_out[4];
  assign    p1_out_mux[5] = (p1_altfunc[5]) ? uart2_txd : p1_out[5];
  assign    p1_out_mux[6] =                               p1_out[6];
  assign    p1_out_mux[7] = (p1_altfunc[7]) ? uart3_txd : p1_out[7];
  assign    p1_out_mux[8] =                               p1_out[8];
  assign    p1_out_mux[9] =                               p1_out[9];
  assign    p1_out_mux[10] = (p1_altfunc[10]) ? i2c0_scl  : p1_out[10];
  assign    p1_out_mux[11] = (p1_altfunc[11]) ? i2c0_sda  : p1_out[11];
  assign    p1_out_mux[12] = (p1_altfunc[12]) ? i2c1_scl  : p1_out[12];
  assign    p1_out_mux[13] = (p1_altfunc[13]) ? i2c1_sda  : p1_out[13];
  assign    p1_out_mux[15:14] = p1_out[15:14];

  // Output enable mux
  assign    p0_out_en_mux[0] = (p0_altfunc[0]) ? ssp0_clk_out_en_n : p0_outen[0];
  assign    p0_out_en_mux[1] = (p0_altfunc[1]) ? ssp0_clk_out_en_n : p0_outen[1];
  assign    p0_out_en_mux[2] = (p0_altfunc[2]) ? ssp0_data_out_en_n : p0_outen[2];
  assign    p0_out_en_mux[3] =                                       p0_outen[3];
  assign    p0_out_en_mux[4] = (p0_altfunc[4]) ? ssp1_clk_out_en_n : p0_outen[4];
  assign    p0_out_en_mux[5] = (p0_altfunc[5]) ? ssp1_clk_out_en_n : p0_outen[5];
  assign    p0_out_en_mux[6] = (p0_altfunc[6]) ? ssp1_data_out_en_n : p0_outen[6];
  assign    p0_out_en_mux[7] =                                       p0_outen[7];
  assign    p0_out_en_mux[15:8] = p0_outen[15:8];

  assign    p1_out_en_mux[0] =                                p1_outen[0];
  assign    p1_out_en_mux[1] = (p1_altfunc[1]) ? uart0_txen : p1_outen[1];
  assign    p1_out_en_mux[2] =                                p1_outen[2];
  assign    p1_out_en_mux[3] = (p1_altfunc[3]) ? uart1_txen : p1_outen[3];
  assign    p1_out_en_mux[4] =                                p1_outen[4];
  assign    p1_out_en_mux[5] = (p1_altfunc[5]) ? uart2_txen : p1_outen[5];
  assign    p1_out_en_mux[6] =                                p1_outen[6];
  assign    p1_out_en_mux[7] = (p1_altfunc[7]) ? uart3_txen : p1_outen[7];
  assign    p1_out_en_mux[8] =                                p1_outen[8];
  assign    p1_out_en_mux[9] =                                p1_outen[9];
  assign    p1_out_en_mux[10] = (p1_altfunc[10]) ? i2c0_scl_en_n : p1_outen[10];
  assign    p1_out_en_mux[11] = (p1_altfunc[11]) ? i2c0_sda_en_n : p1_outen[11];
  assign    p1_out_en_mux[12] = (p1_altfunc[12]) ? i2c1_scl_en_n : p1_outen[12];
  assign    p1_out_en_mux[13] = (p1_altfunc[13]) ? i2c1_sda_en_n : p1_outen[13];
  assign    p1_out_en_mux[15:14] = p1_outen[15:14];

  // Output tristate
  assign    P0[ 0] = p0_out_en_mux[ 0] ? p0_out_mux[ 0] : 1'bz;
  assign    P0[ 1] = p0_out_en_mux[ 1] ? p0_out_mux[ 1] : 1'bz;
  assign    P0[ 2] = p0_out_en_mux[ 2] ? p0_out_mux[ 2] : 1'bz;
  assign    P0[ 3] = p0_out_en_mux[ 3] ? p0_out_mux[ 3] : 1'bz;
  assign    P0[ 4] = p0_out_en_mux[ 4] ? p0_out_mux[ 4] : 1'bz;
  assign    P0[ 5] = p0_out_en_mux[ 5] ? p0_out_mux[ 5] : 1'bz;
  assign    P0[ 6] = p0_out_en_mux[ 6] ? p0_out_mux[ 6] : 1'bz;
  assign    P0[ 7] = p0_out_en_mux[ 7] ? p0_out_mux[ 7] : 1'bz;
  assign    P0[ 8] = p0_out_en_mux[ 8] ? p0_out_mux[ 8] : 1'bz;
  assign    P0[ 9] = p0_out_en_mux[ 9] ? p0_out_mux[ 9] : 1'bz;
  assign    P0[10] = p0_out_en_mux[10] ? p0_out_mux[10] : 1'bz;
  assign    P0[11] = p0_out_en_mux[11] ? p0_out_mux[11] : 1'bz;
  assign    P0[12] = p0_out_en_mux[12] ? p0_out_mux[12] : 1'bz;
  assign    P0[13] = p0_out_en_mux[13] ? p0_out_mux[13] : 1'bz;
  assign    P0[14] = p0_out_en_mux[14] ? p0_out_mux[14] : 1'bz;
  assign    P0[15] = p0_out_en_mux[15] ? p0_out_mux[15] : 1'bz;

  assign    P1[ 0] = p1_out_en_mux[ 0] ? p1_out_mux[ 0] : 1'bz;
  assign    P1[ 1] = p1_out_en_mux[ 1] ? p1_out_mux[ 1] : 1'bz;
  assign    P1[ 2] = p1_out_en_mux[ 2] ? p1_out_mux[ 2] : 1'bz;
  assign    P1[ 3] = p1_out_en_mux[ 3] ? p1_out_mux[ 3] : 1'bz;
  assign    P1[ 4] = p1_out_en_mux[ 4] ? p1_out_mux[ 4] : 1'bz;
  assign    P1[ 5] = p1_out_en_mux[ 5] ? p1_out_mux[ 5] : 1'bz;
  assign    P1[ 6] = p1_out_en_mux[ 6] ? p1_out_mux[ 6] : 1'bz;
  assign    P1[ 7] = p1_out_en_mux[ 7] ? p1_out_mux[ 7] : 1'bz;
  assign    P1[ 8] = p1_out_en_mux[ 8] ? p1_out_mux[ 8] : 1'bz;
  assign    P1[ 9] = p1_out_en_mux[ 9] ? p1_out_mux[ 9] : 1'bz;
  assign    P1[10] = p1_out_en_mux[10] ? p1_out_mux[10] : 1'bz;
  assign    P1[11] = p1_out_en_mux[11] ? p1_out_mux[11] : 1'bz;
  assign    P1[12] = p1_out_en_mux[12] ? p1_out_mux[12] : 1'bz;
  assign    P1[13] = p1_out_en_mux[13] ? p1_out_mux[13] : 1'bz;
  assign    P1[14] = p1_out_en_mux[14] ? p1_out_mux[14] : 1'bz;
  assign    P1[15] = p1_out_en_mux[15] ? p1_out_mux[15] : 1'bz;

// synopsys translate_off

  // Pullup
  pullup(P0[ 0]);
  pullup(P0[ 1]);
  pullup(P0[ 2]);
  pullup(P0[ 3]);
  pullup(P0[ 4]);
  pullup(P0[ 5]);
  pullup(P0[ 6]);
  pullup(P0[ 7]);
  pullup(P0[ 8]);
  pullup(P0[ 9]);
  pullup(P0[10]);
  pullup(P0[11]);
  pullup(P0[12]);
  pullup(P0[13]);
  pullup(P0[14]);
  pullup(P0[15]);

  pullup(P1[ 0]);
  pullup(P1[ 1]);
  pullup(P1[ 2]);
  pullup(P1[ 3]);
  pullup(P1[ 4]);
  pullup(P1[ 5]);
  pullup(P1[ 6]);
  pullup(P1[ 7]);
  pullup(P1[ 8]);
  pullup(P1[ 9]);
  pullup(P1[10]);
  pullup(P1[11]);
  pullup(P1[12]);
  pullup(P1[13]);
  pullup(P1[14]);
  pullup(P1[15]);

// synopsys translate_on

  //-------------------------------------------
  // Debug connections
  //-------------------------------------------

  assign    i_trst_n     = nTRST;
  assign    i_tdi        = TDI;
  assign    i_swclktck   = SWCLKTCK;
  assign    i_swditms    = SWDIOTMS;

  // Tristate buffers for debug output signals
  bufif0 (TDO,      i_tdo,  i_tdoen_n);
  bufif1 (SWDIOTMS, i_swdo, i_swdoen);

  endmodule
