//-----------------------------------------------------------------------------
// The confidential and proprietary information contained in this file may
// only be used by a person authorised under and to the extent permitted
// by a subsisting licensing agreement from ARM Limited.
//
//            (C) COPYRIGHT 2010-2013 ARM Limited.
//                ALL RIGHTS RESERVED
//
// This entire notice must be reproduced on all copies of this file
// and copies of this file may only be made by a person if such person is
// permitted to do so under the terms of a subsisting license agreement
// from ARM Limited.
//
//      SVN Information
//
//      Checked In          : $Date: 2013-04-12 18:34:22 +0100 (Fri, 12 Apr 2013) $
//
//      Revision            : $Revision: 243882 $
//
//      Release Information : Cortex-M System Design Kit-r1p0-00rel0
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Abstract : APB sub system
//-----------------------------------------------------------------------------
module cmsdk_apb_subsystem_m0ds #(
  // Enable setting for APB extension ports
  // By default, all four extension ports are not used.
  // This can be overriden by parameters at instantiations.
  parameter  APB_EXT_PORT13_ENABLE=0,
  parameter  APB_EXT_PORT14_ENABLE=0,
  parameter  APB_EXT_PORT15_ENABLE=0,

  // If peripherals are generated with asynchronous clock domain to HCLK of the processor
  // You might need to add synchroniser to the IRQ signal.
  // In this example APB subsystem, the IRQ synchroniser is used to all peripherals
  // when the INCLUDE_IRQ_SYNCHRONIZER parameter is set to 1. In practice you may have
  // some IRQ signals need to be synchronised and some do not.
  parameter  INCLUDE_IRQ_SYNCHRONIZER=0,

  // By default the APB subsystem include a simple test slave use in ARM for
  // validation purpose.  You can remove this test slave by setting the
  // INCLUDE_APB_TEST_SLAVE paramater to 0,
  

  // Parameter options for including peripherals
  parameter  INCLUDE_APB_TIMER0     = 0,  // Include simple timer #0
  parameter  INCLUDE_APB_TIMER1     = 0,  // Include simple timer #1
  parameter  INCLUDE_APB_DUALTIMER0 = 0,  // Include dual timer module
  parameter  INCLUDE_APB_WATCHDOG   = 0,  // Include APB watchdog module
  parameter  INCLUDE_APB_UART0      = 0,  // Include UART #0
  parameter  INCLUDE_APB_UART1      = 0,  // Include UART #1
  parameter  INCLUDE_APB_UART2      = 0,  // Include UART #2.
                                          // Note : UART #2 is required for text messages
                                          //        display and to enable debug tester in
                                          //        debug tests
  parameter  INCLUDE_APB_UART3      = 0,  // Include UART #3.
  parameter  INCLUDE_APB_SSP0       = 0,  // Include SSP #0.
  parameter  INCLUDE_APB_SSP1       = 0,  // Include SSP #1.
  parameter  INCLUDE_APB_I2C0       = 0,  // Include I2C #0.
  parameter  INCLUDE_APB_I2C1       = 0,  // Include I2C #1.
  parameter  INCLUDE_APB_ADC         =0,  // Include ADC.


  // Big endian - Add additional endian conversion logic to support big endian.
  //              (for ARM internal testing and evaluation of the processor in
  //              big endian configuration).
  //              0 = little endian, 1 = big endian
  //
  //              The example systems including this APB subsystem are designed as
  //              little endian. Most of the peripherals and memory system are
  //              little endian. This parameter is introduced to allows ARM to
  //              perform system level tests to verified behaviour of bus
  //              components in big endian configuration, and to allow designers
  //              to evaluate the processor in big endian configuration.
  //
  //              Use of this parameter is not recommended for actual product
  //              development as this adds extra hardware. For big endian systems
  //              ideally the peripherals should be modified to use a big endian
  //              programmer's model.
  parameter  BE = 0)
 (
// --------------------------------------------------------------------------
// Port Definitions
// --------------------------------------------------------------------------
  // AHB interface for AHB to APB bridge

  input  wire           HCLK,
  input  wire           HRESETn,

  input  wire           HSEL,
  input  wire   [15:0]  HADDR,
  input  wire    [1:0]  HTRANS,
  input  wire           HWRITE,
  input  wire    [2:0]  HSIZE,
  input  wire    [3:0]  HPROT,
  input  wire           HREADY,
  input  wire   [31:0]  HWDATA,
  output wire           HREADYOUT,
  output wire   [31:0]  HRDATA,
  output wire           HRESP,

  input  wire           PCLK,    // Peripheral clock
  input  wire           PCLKG,   // Gate PCLK for bus interface only
  input  wire           PCLKEN,  // Clock divider for AHB to APB bridge
  input  wire           PRESETn, // APB reset

  output wire   [11:0]  PADDR,
  output wire           PWRITE,
  output wire   [31:0]  PWDATA,
  output wire           PENABLE,

  output wire           ext13_psel,
  output wire           ext14_psel,
  output wire           ext15_psel,

  input  wire   [31:0]  ext13_prdata,
  input  wire           ext13_pready,
  input  wire           ext13_pslverr,

  input  wire   [31:0]  ext14_prdata,
  input  wire           ext14_pready,
  input  wire           ext14_pslverr,

  input  wire   [31:0]  ext15_prdata,
  input  wire           ext15_pready,
  input  wire           ext15_pslverr,

  output wire           APBACTIVE,

  // Peripherals
  // UART
  input  wire           uart0_rxd,
  output wire           uart0_txd,
  output wire           uart0_txen,

  input  wire           uart1_rxd,
  output wire           uart1_txd,
  output wire           uart1_txen,

  input  wire           uart2_rxd,
  output wire           uart2_txd,
  output wire           uart2_txen,
  
  input  wire           uart3_rxd,
  output wire           uart3_txd,
  output wire           uart3_txen,

  // SSP
  input  wire           ssp0_din,
  output wire           ssp0_dout,
  output wire           ssp0_dout_en_n,
  input  wire           ssp0_clk_in,
  output wire           ssp0_clk_out,
  output wire           ssp0_clk_out_en_n,
  input  wire           ssp0_fss_in,
  output wire           ssp0_fss_out,

  input  wire           ssp1_din,
  output wire           ssp1_dout,
  output wire           ssp1_dout_en_n,
  input  wire           ssp1_clk_in,
  output wire           ssp1_clk_out,
  output wire           ssp1_clk_out_en_n,
  input  wire           ssp1_fss_in,
  output wire           ssp1_fss_out,

  // I2C
  inout  wire           i2c0_scl,
  inout  wire           i2c0_sda,
  output wire           i2c0_scl_en_n,
  output wire           i2c0_sda_en_n,

  inout  wire           i2c1_scl,
  inout  wire           i2c1_sda,
  output wire           i2c1_scl_en_n,
  output wire           i2c1_sda_en_n, 
  
  // Timer
  input  wire           timer0_extin,
  input  wire           timer1_extin,

  // Interrupt outputs
  output wire   [31:0]  apbsubsys_interrupt,
  output wire           watchdog_interrupt,
  output wire           watchdog_reset);

  // --------------------------------------------------------------------------
  // Internal wires
  // --------------------------------------------------------------------------
  wire     [15:0]  i_paddr;
  wire             i_psel;
  wire             i_penable;
  wire             i_pwrite;
  wire     [2:0]   i_pprot;
  wire     [3:0]   i_pstrb;
  wire     [31:0]  i_pwdata;

  // wire from APB slave mux to APB bridge
  wire             i_pready_mux;
  wire     [31:0]  i_prdata_mux;
  wire             i_pslverr_mux;

  // Peripheral signals
  wire             timer0_psel;
  wire     [31:0]  timer0_prdata;
  wire             timer0_pready;
  wire             timer0_pslverr;

  wire             timer1_psel;
  wire     [31:0]  timer1_prdata;
  wire             timer1_pready;
  wire             timer1_pslverr;

  wire             dualtimer0_psel;
  wire     [31:0]  dualtimer0_prdata;
  wire             dualtimer0_pready;
  wire             dualtimer0_pslverr;

  wire             watchdog_psel;
  wire     [31:0]  watchdog_prdata;
  wire             watchdog_pready;
  wire             watchdog_pslverr;

  wire             uart0_psel;
  wire     [31:0]  uart0_prdata;
  wire             uart0_pready;
  wire             uart0_pslverr;

  wire             uart1_psel;
  wire     [31:0]  uart1_prdata;
  wire             uart1_pready;
  wire             uart1_pslverr;

  wire             uart2_psel;
  wire     [31:0]  uart2_prdata;
  wire             uart2_pready;
  wire             uart2_pslverr;
  
  wire             uart3_psel;
  wire     [31:0]  uart3_prdata;
  wire             uart3_pready;
  wire             uart3_pslverr;

  wire             ssp0_psel;
  wire             ssp0_pready;
  wire     [31:0]  ssp0_prdata;
  wire             ssp0_pslverr;
  
  wire             ssp1_psel;
  wire             ssp1_pready;
  wire     [31:0]  ssp1_prdata;
  wire             ssp1_pslverr;

  wire             i2c0_psel;
  wire             i2c0_pready;
  wire     [31:0]  i2c0_prdata;
  wire             i2c0_pslverr;
  
  wire             i2c1_psel;
  wire             i2c1_pready;
  wire     [31:0]  i2c1_prdata;
  wire             i2c1_pslverr;

  wire             adc_psel;
  wire             adc_pready;
  wire     [31:0]  adc_prdata;
  wire             adc_pslverr;

  // Interrupt signals from peripherals
  wire             timer0_int;
  wire             timer1_int;
  wire             dualtimer0a_int;
  wire             dualtimer0b_int;
  wire             dualtimer0_comb_int;

  wire             uart0_txint;
  wire             uart0_rxint;
  wire             uart0_txovrint;
  wire             uart0_rxovrint;
  wire             uart0_combined_int;

  wire             uart1_txint;
  wire             uart1_rxint;
  wire             uart1_txovrint;
  wire             uart1_rxovrint;
  wire             uart1_combined_int;

  wire             uart2_txint;
  wire             uart2_rxint;
  wire             uart2_txovrint;
  wire             uart2_rxovrint;
  wire             uart2_combined_int;
  
  wire             uart3_txint;
  wire             uart3_rxint;
  wire             uart3_txovrint;
  wire             uart3_rxovrint;
  wire             uart3_combined_int;

  wire             uart0_overflow_int;
  wire             uart1_overflow_int;
  wire             uart2_overflow_int;
  wire             uart3_overflow_int;
 
  wire             ssp0_interrupt;   
  wire             ssp1_interrupt;
  
  wire             i2c0_int_rx;
  wire             i2c0_int_tx;
  wire             i2c1_int_rx;
  wire             i2c1_int_tx;

  wire             watchdog_int;
  wire             watchdog_rst;

  // Synchronized interrupt signals
  wire             i_uart0_txint;
  wire             i_uart0_rxint;
  wire             i_uart0_overflow_int;
  wire             i_uart1_txint;
  wire             i_uart1_rxint;
  wire             i_uart1_overflow_int;
  wire             i_uart2_txint;
  wire             i_uart2_rxint;
  wire             i_uart2_overflow_int;
  wire             i_uart3_txint;
  wire             i_uart3_rxint;
  wire             i_uart3_overflow_int;
  wire             i_ssp0_interrupt;
  wire             i_ssp1_interrupt;
  wire             i_i2c0_int_rx;
  wire             i_i2c0_int_tx;
  wire             i_i2c1_int_rx;
  wire             i_i2c1_int_tx;
  wire             i_timer0_int;
  wire             i_timer1_int;
  wire             i_dualtimer0_int;
  wire             i_watchdog_int;
  wire             i_watchdog_rst;

  // endian handling
  wire             bigendian;
  assign           bigendian = (BE!=0) ? 1'b1 : 1'b0;

  wire   [31:0]    hwdata_le; // Little endian write data
  wire   [31:0]    hrdata_le; // Little endian read data
  wire             reg_be_swap_ctrl_en = HSEL & HTRANS[1] & HREADY & bigendian;
  reg     [1:0]    reg_be_swap_ctrl; // registered byte swap control
  wire    [1:0]    nxt_be_swap_ctrl; // next state of byte swap control

  assign nxt_be_swap_ctrl[1] = bigendian & (HSIZE[1:0]==2'b10); // Swap upper and lower half word
  assign nxt_be_swap_ctrl[0] = bigendian & (HSIZE[1:0]!=2'b00); // Swap byte within halfword

  // Register byte swap control for data phase
  always @(posedge HCLK or negedge HRESETn)
    begin
    if (~HRESETn)
      reg_be_swap_ctrl <= 2'b00;
    else if (reg_be_swap_ctrl_en)
      reg_be_swap_ctrl <= nxt_be_swap_ctrl;
    end

  // swap byte within half word
  wire  [31:0] hwdata_mux_1 = (reg_be_swap_ctrl[0] & bigendian) ?
     {HWDATA[23:16],HWDATA[31:24],HWDATA[7:0],HWDATA[15:8]}:
     {HWDATA[31:24],HWDATA[23:16],HWDATA[15:8],HWDATA[7:0]};
  // swap lower and upper half word
  assign       hwdata_le    = (reg_be_swap_ctrl[1] & bigendian) ?
     {hwdata_mux_1[15: 0],hwdata_mux_1[31:16]}:
     {hwdata_mux_1[31:16],hwdata_mux_1[15:0]};
  // swap byte within half word
  wire  [31:0] hrdata_mux_1 = (reg_be_swap_ctrl[0] & bigendian) ?
     {hrdata_le[23:16],hrdata_le[31:24],hrdata_le[ 7:0],hrdata_le[15:8]}:
     {hrdata_le[31:24],hrdata_le[23:16],hrdata_le[15:8],hrdata_le[7:0]};
  // swap lower and upper half word
  assign       HRDATA       = (reg_be_swap_ctrl[1] & bigendian) ?
     {hrdata_mux_1[15: 0],hrdata_mux_1[31:16]}:
     {hrdata_mux_1[31:16],hrdata_mux_1[15:0]};

  // AHB to APB bus bridge
  cmsdk_ahb_to_apb
  #(.ADDRWIDTH      (16),
    .REGISTER_RDATA (1),
    .REGISTER_WDATA (0))
  u_ahb_to_apb(
    // AHB side
    .HCLK     (HCLK),
    .HRESETn  (HRESETn),
    .HSEL     (HSEL),
    .HADDR    (HADDR[15:0]),
    .HTRANS   (HTRANS),
    .HSIZE    (HSIZE),
    .HPROT    (HPROT),
    .HWRITE   (HWRITE),
    .HREADY   (HREADY),
    .HWDATA   (hwdata_le),

    .HREADYOUT(HREADYOUT), // AHB Outputs
    .HRDATA   (hrdata_le),
    .HRESP    (HRESP),

    .PADDR    (i_paddr[15:0]),
    .PSEL     (i_psel),
    .PENABLE  (i_penable),
    .PSTRB    (i_pstrb),
    .PPROT    (i_pprot),
    .PWRITE   (i_pwrite),
    .PWDATA   (i_pwdata),

    .APBACTIVE(APBACTIVE),
    .PCLKEN   (PCLKEN),     // APB clock enable signal

    .PRDATA   (i_prdata_mux),
    .PREADY   (i_pready_mux),
    .PSLVERR  (i_pslverr_mux)
    );

  // APB slave multiplexer
  cmsdk_apb_slave_mux
    #( // Parameter to determine which ports are used
    .PORT0_ENABLE  (INCLUDE_APB_TIMER0), // timer 0
    .PORT1_ENABLE  (INCLUDE_APB_TIMER1), // timer 1
    .PORT2_ENABLE  (INCLUDE_APB_DUALTIMER0), // dual timer 0
    .PORT3_ENABLE  (INCLUDE_APB_WATCHDOG), // watchdog
    .PORT4_ENABLE  (INCLUDE_APB_UART0), // uart 0
    .PORT5_ENABLE  (INCLUDE_APB_UART1), // uart 1
    .PORT6_ENABLE  (INCLUDE_APB_UART2), // uart 2
    .PORT7_ENABLE  (INCLUDE_APB_UART3), // uart 3 (added for DesignStart)
    .PORT8_ENABLE  (INCLUDE_APB_SSP0), // ssp 0
    .PORT9_ENABLE  (INCLUDE_APB_SSP1), // ssp 1
    .PORT10_ENABLE (INCLUDE_APB_I2C0), // i2c 0
    .PORT11_ENABLE (INCLUDE_APB_I2C1), // i2c 1
    .PORT12_ENABLE (INCLUDE_APB_ADC),
    .PORT13_ENABLE (APB_EXT_PORT13_ENABLE),
    .PORT14_ENABLE (APB_EXT_PORT14_ENABLE),
    .PORT15_ENABLE (APB_EXT_PORT15_ENABLE)
    )
    u_apb_slave_mux (
    // Inputs
    .DECODE4BIT        (i_paddr[15:12]),
    .PSEL              (i_psel),
    // PSEL (output) and return status & data (inputs) for each port
    .PSEL0             (timer0_psel),
    .PREADY0           (timer0_pready),
    .PRDATA0           (timer0_prdata),
    .PSLVERR0          (timer0_pslverr),

    .PSEL1             (timer1_psel),
    .PREADY1           (timer1_pready),
    .PRDATA1           (timer1_prdata),
    .PSLVERR1          (timer1_pslverr),

    .PSEL2             (dualtimer0_psel),
    .PREADY2           (dualtimer0_pready),
    .PRDATA2           (dualtimer0_prdata),
    .PSLVERR2          (dualtimer0_pslverr),

    .PSEL3             (watchdog_psel),
    .PREADY3           (watchdog_pready),
    .PRDATA3           (watchdog_prdata),
    .PSLVERR3          (watchdog_pslverr),

    .PSEL4             (uart0_psel),
    .PREADY4           (uart0_pready),
    .PRDATA4           (uart0_prdata),
    .PSLVERR4          (uart0_pslverr),

    .PSEL5             (uart1_psel),
    .PREADY5           (uart1_pready),
    .PRDATA5           (uart1_prdata),
    .PSLVERR5          (uart1_pslverr),

    .PSEL6             (uart2_psel),
    .PREADY6           (uart2_pready),
    .PRDATA6           (uart2_prdata),
    .PSLVERR6          (uart2_pslverr),

    .PSEL7             (uart3_psel),
    .PREADY7           (uart3_pready),
    .PRDATA7           (uart3_prdata),
    .PSLVERR7          (uart3_pslverr),

    .PSEL8             (ssp0_psel),
    .PREADY8           (ssp0_pready),
    .PRDATA8           (ssp0_prdata),
    .PSLVERR8          (ssp0_pslverr),

    .PSEL9             (ssp1_psel),
    .PREADY9           (ssp1_pready),
    .PRDATA9           (ssp1_prdata),
    .PSLVERR9          (ssp1_pslverr),

    .PSEL10            (i2c0_psel),
    .PREADY10          (i2c0_pready),
    .PRDATA10          (i2c0_prdata),
    .PSLVERR10         (i2c0_pslverr),

    .PSEL11            (i2c1_psel),
    .PREADY11          (i2c1_pready),
    .PRDATA11          (i2c1_prdata),
    .PSLVERR11         (i2c1_pslverr),

    .PSEL12            (adc_psel),
    .PREADY12          (adc_pready),
    .PRDATA12          (adc_prdata),
    .PSLVERR12         (adc_pslverr),

    .PSEL13            (ext13_psel),
    .PREADY13          (ext13_pready),
    .PRDATA13          (ext13_prdata),
    .PSLVERR13         (ext13_pslverr),

    .PSEL14            (ext14_psel),
    .PREADY14          (ext14_pready),
    .PRDATA14          (ext14_prdata),
    .PSLVERR14         (ext14_pslverr),

    .PSEL15            (ext15_psel),
    .PREADY15          (ext15_pready),
    .PRDATA15          (ext15_prdata),
    .PSLVERR15         (ext15_pslverr),

    // Output
    .PREADY            (i_pready_mux),
    .PRDATA            (i_prdata_mux),
    .PSLVERR           (i_pslverr_mux)
    );

  // -----------------------------------------------------------------
  // Timers

  generate if (INCLUDE_APB_TIMER0 == 1) begin : gen_apb_timer_0
  cmsdk_apb_timer u_apb_timer_0 (
    .PCLK              (PCLK),     // PCLK for timer operation
    .PCLKG             (PCLKG),    // Gated PCLK for bus
    .PRESETn           (PRESETn),  // Reset
    // APB interface inputs
    .PSEL              (timer0_psel),
    .PADDR             (i_paddr[11:2]),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .ECOREVNUM         (4'h0),// Engineering-change-order revision bits

      // APB interface outputs
    .PRDATA            (timer0_prdata),
    .PREADY            (timer0_pready),
    .PSLVERR           (timer0_pslverr),

    .EXTIN             (timer0_extin),  // External input
    .TIMERINT          (timer0_int)     // interrupt output
  );
  end else
  begin : gen_no_apb_timer_0
    assign timer0_prdata  = {32{1'b0}};
    assign timer0_pready  = 1'b1;
    assign timer0_pslverr = 1'b0;
    assign timer0_int     = 1'b0;
  end endgenerate

  generate if (INCLUDE_APB_TIMER1 == 1) begin : gen_apb_timer_1
  cmsdk_apb_timer u_apb_timer_1 (
    .PCLK              (PCLK),     // PCLK for timer operation
    .PCLKG             (PCLKG),    // Gated PCLK for bus
    .PRESETn           (PRESETn),  // Reset
    // APB interface inputs
    .PSEL              (timer1_psel),
    .PADDR             (i_paddr[11:2]),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .ECOREVNUM         (4'h0),// Engineering-change-order revision bits

      // APB interface outputs
    .PRDATA            (timer1_prdata),
    .PREADY            (timer1_pready),
    .PSLVERR           (timer1_pslverr),

    .EXTIN             (timer1_extin),  // External input
    .TIMERINT          (timer1_int)     // interrupt output
  );
  end else
  begin : gen_no_apb_timer_1
    assign timer1_prdata  = {32{1'b0}};
    assign timer1_pready  = 1'b1;
    assign timer1_pslverr = 1'b0;
    assign timer1_int     = 1'b0;
  end endgenerate

  // -----------------------------------------------------------------
  // Dual Timers
  generate if (INCLUDE_APB_DUALTIMER0 == 1) begin : gen_apb_dualtimers_0
  cmsdk_apb_dualtimers u_apb_dualtimers_0 (
   // Inputs
    .PCLK              (PCLKG),
    .PRESETn           (PRESETn),
    .PENABLE           (i_penable),
    .PSEL              (dualtimer0_psel),
    .PADDR             (i_paddr[11:2]),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .TIMCLK            (PCLK),
    .TIMCLKEN1         (1'b1), // simple case:the timer 0 clock always enable
    .TIMCLKEN2         (1'b1), // simple case:the timer 1 clock always enable

    .ECOREVNUM         (4'h0),// Engineering-change-order revision bits

   // Outputs
    .PRDATA            (dualtimer0_prdata),

    .TIMINT1           (dualtimer0a_int), // not used
    .TIMINT2           (dualtimer0b_int), // not used
    .TIMINTC           (dualtimer0_comb_int)

  );
  end else
  begin : gen_no_apb_dualtimers_0
    assign dualtimer0_prdata   = {32{1'b0}};
    assign dualtimer0_comb_int = 1'b0;
    assign dualtimer0a_int     = 1'b0;
    assign dualtimer0b_int     = 1'b0;
  end endgenerate

  // When using peripherals with APB (AMBA 2.0), the PREADY and PSLVERR
  // signals are not required. So we connect PREADY to 1 and PSLVERR to 0.
  assign dualtimer0_pslverr = 1'b0;
  assign dualtimer0_pready  = 1'b1;

  // -----------------------------------------------------------------
  // Watchdog

  generate if (INCLUDE_APB_WATCHDOG == 1) begin : gen_apb_watchdog
  cmsdk_apb_watchdog u_apb_watchdog (
   // Inputs
    .PCLK              (PCLKG),
    .PRESETn           (PRESETn),
    .PENABLE           (i_penable),
    .PSEL              (watchdog_psel),
    .PADDR             (i_paddr[11:2]),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .WDOGCLK           (PCLK),
    .WDOGCLKEN         (1'b1),
    .WDOGRESn          (PRESETn),

    .ECOREVNUM         (4'h0),// Engineering-change-order revision bits

   // Outputs
    .PRDATA            (watchdog_prdata),

    .WDOGINT           (watchdog_int),  // connect to NMI
    .WDOGRES           (watchdog_rst)   // connect to reset generator

  );
  end else
  begin : gen_no_apb_watchdog
    assign watchdog_prdata  = {32{1'b0}};
    assign watchdog_int     = 1'b0;
    assign watchdog_rst     = 1'b0;
  end endgenerate

  // When using peripherals with APB (AMBA 2.0), the PREADY and PSLVERR
  // signals are not required. So we connect PREADY to 1 and PSLVERR to 0.
  assign watchdog_pslverr = 1'b0;
  assign watchdog_pready  = 1'b1;

  // -----------------------------------------------------------------
  // UARTs
  generate if (INCLUDE_APB_UART0 == 1) begin : gen_apb_uart_0
  cmsdk_apb_uart u_apb_uart_0 (
    .PCLK              (PCLK),     // Peripheral clock
    .PCLKG             (PCLKG),    // Gated PCLK for bus
    .PRESETn           (PRESETn),  // Reset

    .PSEL              (uart0_psel),     // APB interface inputs
    .PADDR             (i_paddr[11:2]),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .PRDATA            (uart0_prdata),   // APB interface outputs
    .PREADY            (uart0_pready),
    .PSLVERR           (uart0_pslverr),

    .ECOREVNUM         (4'h0),// Engineering-change-order revision bits

    .RXD               (uart0_rxd),      // Receive data

    .TXD               (uart0_txd),      // Transmit data
    .TXEN              (uart0_txen),     // Transmit Enabled

    .BAUDTICK          (),   // Baud rate x16 tick output (for testing)

    .TXINT             (uart0_txint),       // Transmit Interrupt
    .RXINT             (uart0_rxint),       // Receive  Interrupt
    .TXOVRINT          (uart0_txovrint),    // Transmit Overrun Interrupt
    .RXOVRINT          (uart0_rxovrint),    // Receive  Overrun Interrupt
    .UARTINT           (uart0_combined_int) // Combined Interrupt (not used)
  );
  end else
  begin : gen_no_apb_uart_0
    assign uart0_prdata  = {32{1'b0}};
    assign uart0_pready  = 1'b1;
    assign uart0_pslverr = 1'b0;
    assign uart0_txd     = 1'b1;
    assign uart0_txen    = 1'b0;
    assign uart0_txint   = 1'b0;
    assign uart0_rxint   = 1'b0;
    assign uart0_txovrint = 1'b0;
    assign uart0_rxovrint = 1'b0;
    assign uart0_combined_int = 1'b0;
  end endgenerate

  generate if (INCLUDE_APB_UART1 == 1) begin : gen_apb_uart_1
  cmsdk_apb_uart u_apb_uart_1 (
    .PCLK              (PCLK),     // Peripheral clock
    .PCLKG             (PCLKG),    // Gated PCLK for bus
    .PRESETn           (PRESETn),  // Reset

    .PSEL              (uart1_psel),     // APB interface inputs
    .PADDR             (i_paddr[11:2]),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .PRDATA            (uart1_prdata),   // APB interface outputs
    .PREADY            (uart1_pready),
    .PSLVERR           (uart1_pslverr),

    .ECOREVNUM         (4'h0),// Engineering-change-order revision bits

    .RXD               (uart1_rxd),      // Receive data

    .TXD               (uart1_txd),      // Transmit data
    .TXEN              (uart1_txen),     // Transmit Enabled

    .BAUDTICK          (),   // Baud rate x16 tick output (for testing)

    .TXINT             (uart1_txint),       // Transmit Interrupt
    .RXINT             (uart1_rxint),       // Receive  Interrupt
    .TXOVRINT          (uart1_txovrint),    // Transmit Overrun Interrupt
    .RXOVRINT          (uart1_rxovrint),    // Receive  Overrun Interrupt
    .UARTINT           (uart1_combined_int) // Combined Interrupt (not used)
  );
  end else
  begin : gen_no_apb_uart_1
    assign uart1_prdata  = {32{1'b0}};
    assign uart1_pready  = 1'b1;
    assign uart1_pslverr = 1'b0;
    assign uart1_txd     = 1'b1;
    assign uart1_txen    = 1'b0;
    assign uart1_txint   = 1'b0;
    assign uart1_rxint   = 1'b0;
    assign uart1_txovrint = 1'b0;
    assign uart1_rxovrint = 1'b0;
    assign uart1_combined_int = 1'b0;
  end endgenerate

  generate if (INCLUDE_APB_UART2 == 1) begin : gen_apb_uart_2
  cmsdk_apb_uart u_apb_uart_2 (
    .PCLK              (PCLK),     // Peripheral clock
    .PCLKG             (PCLKG),    // Gated PCLK for bus
    .PRESETn           (PRESETn),  // Reset

    .PSEL              (uart2_psel),     // APB interface inputs
    .PADDR             (i_paddr[11:2]),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .PRDATA            (uart2_prdata),   // APB interface outputs
    .PREADY            (uart2_pready),
    .PSLVERR           (uart2_pslverr),

    .ECOREVNUM         (4'h0),// Engineering-change-order revision bits

    .RXD               (uart2_rxd),      // Receive data

    .TXD               (uart2_txd),      // Transmit data
    .TXEN              (uart2_txen),     // Transmit Enabled

    .BAUDTICK          (),   // Baud rate x16 tick output (for testing)

    .TXINT             (uart2_txint),       // Transmit Interrupt
    .RXINT             (uart2_rxint),       // Receive  Interrupt
    .TXOVRINT          (uart2_txovrint),    // Transmit Overrun Interrupt
    .RXOVRINT          (uart2_rxovrint),    // Receive  Overrun Interrupt
    .UARTINT           (uart2_combined_int) // Combined Interrupt (not used)
  );
  end else
  begin : gen_no_apb_uart_2
    assign uart2_prdata  = {32{1'b0}};
    assign uart2_pready  = 1'b1;
    assign uart2_pslverr = 1'b0;
    assign uart2_txd     = 1'b1;
    assign uart2_txen    = 1'b0;
    assign uart2_txint   = 1'b0;
    assign uart2_rxint   = 1'b0;
    assign uart2_txovrint = 1'b0;
    assign uart2_rxovrint = 1'b0;
    assign uart2_combined_int = 1'b0;
  end endgenerate


  generate if (INCLUDE_APB_UART3 == 1) begin : gen_apb_uart_3 
  cmsdk_apb_uart u_apb_uart_3 (
    .PCLK              (PCLK),              // Peripheral clock
    .PCLKG             (PCLKG),             // Gated PCLK for bus
    .PRESETn           (PRESETn),           // Reset

    .PSEL              (uart3_psel),     // APB interface inputs
    .PADDR             (i_paddr[11:2]),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),
    .PWDATA            (i_pwdata),

    .PRDATA            (uart3_prdata),   // APB interface outputs
    .PREADY            (uart3_pready),
    .PSLVERR           (uart3_pslverr),

    .ECOREVNUM         (4'h0),              // Engineering-change-order revision bits

    .RXD               (uart3_rxd),         // Receive data

    .TXD               (uart3_txd),         // Transmit data
    .TXEN              (uart3_txen),        // Transmit Enabled

    .BAUDTICK          (),                  // Baud rate x16 tick output (for testing)

    .TXINT             (uart3_txint),       // Transmit Interrupt
    .RXINT             (uart3_rxint),       // Receive  Interrupt
    .TXOVRINT          (uart3_txovrint),    // Transmit Overrun Interrupt
    .RXOVRINT          (uart3_rxovrint),    // Receive  Overrun Interrupt
    .UARTINT           (uart3_combined_int) // Combined Interrupt (not used)
  );
  end else
  begin : gen_no_apb_uart_3
    assign uart3_prdata  = {32{1'b0}};
    assign uart3_pready  = 1'b1;
    assign uart3_pslverr = 1'b0;
    assign uart3_txd     = 1'b1;
    assign uart3_txen    = 1'b0;
    assign uart3_txint   = 1'b0;
    assign uart3_rxint   = 1'b0;
    assign uart3_txovrint = 1'b0;
    assign uart3_rxovrint = 1'b0;
    assign uart3_combined_int = 1'b0;
  end endgenerate
  
  
  generate if (INCLUDE_APB_SSP0 == 1) begin : gen_apb_ssp_0    
  Ssp u_ssp_0 (
    .PCLK              (PCLK),
    .SSPCLK            (PCLK),

    .PRESETn           (PRESETn),
    .nSSPRST           (PRESETn),
    .PSEL              (ssp0_psel),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),

    // - Frame clock. This is acting as master so Clock in not used
    .SSPCLKIN          (ssp0_clk_in),
    // - Frame Select Signal(s).  This is the Master so FSSIN is tied low.
    .SSPFSSIN          (ssp0_fss_in),
    // - Frame data in
    .SSPRXD            (ssp0_din),

    .SCANENABLE        (1'b0),
    .SCANINPCLK        (1'b0),
    .SCANINSSPCLK      (1'b0),

    .PADDR             (i_paddr[11:2]),

    .PWDATA            (i_pwdata[15:0]),

    .SSPTXDMACLR       (1'b0),
    .SSPRXDMACLR       (1'b0),
    // Outputs
    .SSPINTR           (ssp0_interrupt),
    .SSPRXINTR         (),
    .SSPTXINTR         (),
    .SSPRORINTR        (),
    .SSPRTINTR         (),

    .SSPFSSOUT         (ssp0_fss_out),
    .SSPCLKOUT         (ssp0_clk_out),
    .nSSPCTLOE         (ssp0_clk_out_en_n),

    .SCANOUTPCLK       (),
    .SCANOUTSSPCLK     (),

    .SSPTXD            (ssp0_dout),
    .nSSPOE            (ssp0_dout_en_n),


    .PRDATA            (ssp0_prdata[15:0]),

    .SSPTXDMASREQ      (),
    .SSPTXDMABREQ      (),
    .SSPRXDMASREQ      (),
    .SSPRXDMABREQ      ()
  );
    assign ssp0_pready   = 1'b1;
    assign ssp0_pslverr  = 1'b0;
    assign ssp0_prdata[31:16] = {16{1'b0}};
  end else
  begin : gen_no_apb_ssp_0
    assign ssp0_prdata   = {32{1'b0}};
    assign ssp0_pready   = 1'b1;
    assign ssp0_pslverr  = 1'b0;
    assign ssp0_dout     = 1'b0;
    assign ssp0_dout_en_n = 1'b0;
    assign ssp0_clk_out  = 1'b0;
    assign ssp0_clk_out_en_n = 1'b0;
    assign ssp0_interrupt = 1'b0;
    assign ssp0_fss_out = 1'b0;
  end endgenerate

  generate if (INCLUDE_APB_SSP1 == 1) begin : gen_apb_ssp_1    
  Ssp u_ssp_1 (
    .PCLK              (PCLK),
    .SSPCLK            (PCLK),

    .PRESETn           (PRESETn),
    .nSSPRST           (PRESETn),

    .PSEL              (ssp1_psel),
    .PENABLE           (i_penable),
    .PWRITE            (i_pwrite),

    // - Frame clock. This is acting as master so Clock in not used
    .SSPCLKIN          (ssp1_clk_in),
    // - Frame Select Signal(s).  This is the Master so FSSIN is tied low.
    .SSPFSSIN          (ssp1_fss_in),
    // - Frame data in
    .SSPRXD            (ssp1_din),

    .SCANENABLE        (1'b0),
    .SCANINPCLK        (1'b0),
    .SCANINSSPCLK      (1'b0),

    .PADDR             (i_paddr[11:2]),

    .PWDATA            (i_pwdata[15:0]),

    .SSPTXDMACLR       (1'b0),
    .SSPRXDMACLR       (1'b0),
    // Outputs
    .SSPINTR           (ssp1_interrupt),
    .SSPRXINTR         (),
    .SSPTXINTR         (),
    .SSPRORINTR        (),
    .SSPRTINTR         (),

    .SSPFSSOUT         (ssp1_fss_out),
    .SSPCLKOUT         (ssp1_clk_out),
    .nSSPCTLOE         (ssp1_clk_out_en_n),

    .SCANOUTPCLK       (),
    .SCANOUTSSPCLK     (),

    .SSPTXD            (ssp1_dout),
    .nSSPOE            (ssp1_dout_en_n),


    .PRDATA            (ssp1_prdata[15:0]),

    .SSPTXDMASREQ      (),
    .SSPTXDMABREQ      (),
    .SSPRXDMASREQ      (),
    .SSPRXDMABREQ      ()
  );
    assign ssp1_pready   = 1'b1;
    assign ssp1_pslverr  = 1'b0;
    assign ssp1_prdata[31:16] = {16{1'b0}};
  end else
  begin : gen_no_apb_ssp_1
    assign ssp1_prdata   = {32{1'b0}};
    assign ssp1_pready   = 1'b1;
    assign ssp1_pslverr  = 1'b0;
    assign ssp1_dout     = 1'b0;
    assign ssp1_dout_en_n = 1'b0;
    assign ssp1_clk_out  = 1'b0;
    assign ssp1_clk_out_en_n = 1'b0;
    assign ssp1_interrupt = 1'b0;
    assign ssp1_fss_out = 1'b0;
  end endgenerate

  generate if (INCLUDE_APB_I2C0 == 1) begin : gen_apb_i2c_0
  i2c u_i2c_0 (
    // Inputs
    .PCLK        (PCLK),           //-- APB bus clock
    .PRESETn     (PRESETn),        //-- APB bus reset
    .PSELx       (i2c0_psel),      //-- APB device select
    .PENABLE     (i_penable),      //-- identifies APB active cycle
    .PWRITE      (i_pwrite),       //-- APB address
    .PADDR       (i_paddr[11:2]),  //-- APB transfer direction
    .PWDATA      (i_pwdata[31:0]), //-- APB write data
    .PREADY      (i2c0_pready),
    .PSLVERR     (i2c0_pslverr),
    
    // Outputs
    .PRDATA      (i2c0_prdata[31:0]),
    .SCL         (i2c0_scl),
    .SDA         (i2c0_sda),
    .SCL_ENABLE  (i2c0_scl_en_n),
    .SDA_ENABLE  (i2c0_sda_en_n),
    .INT_RX      (i2c0_int_rx),
    .INT_TX      (i2c0_int_tx)
  );
  end else
  begin : gen_no_apb_i2c_0
    assign i2c0_prdata  = {32{1'b0}};
    assign i2c0_pslverr = 1'b0;
    assign i2c0_pready  = 1'b1;
    assign i2c0_scl     = 1'b0;
    assign i2c0_sda     = 1'b0;
    assign i2c0_scl_en_n = 1'b0;
    assign i2c0_sda_en_n = 1'b0;
    assign i2c0_int_rx  = 1'b0;
    assign i2c0_int_tx  = 1'b0;
  end endgenerate

  generate if (INCLUDE_APB_I2C1 == 1) begin : gen_apb_i2c_1
  i2c u_i2c_1 (
    // Inputs
    .PCLK        (PCLK),           //-- APB bus clock
    .PRESETn     (PRESETn),        //-- APB bus reset
    .PSELx       (i2c1_psel),      //-- APB device select
    .PENABLE     (i_penable),      //-- identifies APB active cycle
    .PWRITE      (i_pwrite),       //-- APB address
    .PADDR       (i_paddr[11:2]),  //-- APB transfer direction
    .PWDATA      (i_pwdata[31:0]), //-- APB write data
    .PREADY      (i2c1_pready),
    .PSLVERR     (i2c1_pslverr),
    
    // Outputs
    .PRDATA      (i2c1_prdata[31:0]),
    .SCL         (i2c1_scl),
    .SDA         (i2c1_sda),
    .SCL_ENABLE  (i2c1_scl_en_n),
    .SDA_ENABLE  (i2c1_sda_en_n),
    .INT_RX      (i2c1_int_rx),
    .INT_TX      (i2c1_int_tx)
  );
  end else
  begin : gen_no_apb_i2c_1
    assign i2c1_prdata  = {32{1'b0}};
    assign i2c1_pslverr = 1'b0;
    assign i2c1_pready  = 1'b1;
    assign i2c1_scl     = 1'b0;
    assign i2c1_sda     = 1'b0;
    assign i2c1_scl_en_n = 1'b0;
    assign i2c1_sda_en_n = 1'b0;
    assign i2c1_int_rx  = 1'b0;
    assign i2c1_int_tx  = 1'b0;
  end endgenerate

  generate if (INCLUDE_APB_ADC == 1) begin : gen_apb_adc
  fpga_adc u_fpga_adc (
    // Inputs
    .clk         (),
    .pd          (),
    .s           (),
    .soc         (),     
    // Outputs
    .eoc         (),
    .dout        ()
  );
    assign adc_pslverr = 1'b0;
    assign adc_pready  = 1'b1;
  end else
  begin : gen_no_apb_adc
    assign adc_prdata  = {32{1'b0}};
    assign adc_pslverr = 1'b0;
    assign adc_pready  = 1'b1;
  end endgenerate

  // -----------------------------------------------------------------
  // Connection to external
  assign PADDR   = i_paddr[11:0];
  assign PENABLE = i_penable;
  assign PWRITE  = i_pwrite;
  assign PWDATA  = i_pwdata;

  assign uart0_overflow_int = uart0_txovrint|uart0_rxovrint;
  assign uart1_overflow_int = uart1_txovrint|uart1_rxovrint;
  assign uart2_overflow_int = uart2_txovrint|uart2_rxovrint;
  assign uart3_overflow_int = uart3_txovrint|uart3_rxovrint;

  generate if (INCLUDE_IRQ_SYNCHRONIZER == 0) begin : gen_irq_synchroniser
    // If PCLK is synchronous to HCLK, no need to have synchronizers
    assign i_uart0_txint = uart0_txint;
    assign i_uart0_rxint = uart0_rxint;
    assign i_uart1_txint = uart1_txint;
    assign i_uart1_rxint = uart1_rxint;
    assign i_uart2_txint = uart2_txint;
    assign i_uart2_rxint = uart2_rxint;
    assign i_uart3_txint = uart3_txint;
    assign i_uart3_rxint = uart3_rxint;
    assign i_timer0_int  = timer0_int;
    assign i_timer1_int  = timer1_int;
    assign i_dualtimer0_int = dualtimer0_comb_int;
    assign i_uart0_overflow_int = uart0_overflow_int;
    assign i_uart1_overflow_int = uart1_overflow_int;
    assign i_uart2_overflow_int = uart2_overflow_int;
    assign i_uart3_overflow_int = uart3_overflow_int;
    assign i_ssp0_interrupt = ssp0_interrupt;
    assign i_ssp1_interrupt = ssp1_interrupt;
    assign i_i2c0_int_rx = i2c0_int_rx;
    assign i_i2c0_int_tx = i2c0_int_tx;
    assign i_i2c1_int_rx = i2c1_int_rx;
    assign i_i2c1_int_tx = i2c1_int_tx;

    assign i_watchdog_int = watchdog_int;
    assign i_watchdog_rst = watchdog_rst;
  end else
  begin : gen_no_irq_synchroniser
    // If IRQ source are asynchronous to HCLK, then we
    // need to add synchronizers to prevent metastability
    // on interrupt signals.
    cmsdk_irq_sync u_irq_sync_0 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart0_txint),
      .IRQOUT(i_uart0_txint)
      );

    cmsdk_irq_sync u_irq_sync_1 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart0_rxint),
      .IRQOUT(i_uart0_rxint)
      );

    cmsdk_irq_sync u_irq_sync_2 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart1_txint),
      .IRQOUT(i_uart1_txint)
      );

    cmsdk_irq_sync u_irq_sync_3 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart1_rxint),
      .IRQOUT(i_uart1_rxint)
      );

    cmsdk_irq_sync u_irq_sync_4 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart2_txint),
      .IRQOUT(i_uart2_txint)
      );

    cmsdk_irq_sync u_irq_sync_5 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart2_rxint),
      .IRQOUT(i_uart2_rxint)
      );

    cmsdk_irq_sync u_irq_sync_6 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart3_txint),
      .IRQOUT(i_uart3_txint)
      );

    cmsdk_irq_sync u_irq_sync_7 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart3_rxint),
      .IRQOUT(i_uart3_rxint)
      );

    cmsdk_irq_sync u_irq_sync_8 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart0_overflow_int),
      .IRQOUT(i_uart0_overflow_int)
      );

    cmsdk_irq_sync u_irq_sync_9 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart1_overflow_int),
      .IRQOUT(i_uart1_overflow_int)
      );

    cmsdk_irq_sync u_irq_sync_10 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart2_overflow_int),
      .IRQOUT(i_uart2_overflow_int)
      );

    cmsdk_irq_sync u_irq_sync_11 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (uart3_overflow_int),
      .IRQOUT(i_uart3_overflow_int)
      );

    cmsdk_irq_sync u_irq_sync_12 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (timer0_int),
      .IRQOUT(i_timer0_int)
      );

    cmsdk_irq_sync u_irq_sync_13 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (timer1_int),
      .IRQOUT(i_timer1_int)
      );

    cmsdk_irq_sync u_irq_sync_14 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (dualtimer0_comb_int),
      .IRQOUT(i_dualtimer0_int)
      );

    cmsdk_irq_sync u_irq_sync_15 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (watchdog_int),
      .IRQOUT(i_watchdog_int)
      );

    cmsdk_irq_sync u_irq_sync_16 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (watchdog_rst),
      .IRQOUT(i_watchdog_rst)
      );
    

    cmsdk_irq_sync u_irq_sync_17 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (ssp0_interrupt),
      .IRQOUT(i_ssp0_interrupt)
    );

    cmsdk_irq_sync u_irq_sync_18 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (ssp1_interrupt),
      .IRQOUT(i_ssp1_interrupt)
    );
    
    cmsdk_irq_sync u_irq_sync_19 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (i2c0_int_rx),
      .IRQOUT(i_i2c0_int_rx)
    );
    
    cmsdk_irq_sync u_irq_sync_20 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (i2c0_int_tx),
      .IRQOUT(i_i2c0_int_tx)
    );
    
    cmsdk_irq_sync u_irq_sync_21 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (i2c1_int_rx),
      .IRQOUT(i_i2c1_int_rx)
    );
    
    cmsdk_irq_sync u_irq_sync_22 (
      .RSTn  (HRESETn),
      .CLK   (HCLK),
      .IRQIN (i2c1_int_tx),
      .IRQOUT(i_i2c1_int_tx)
    );
    
  end endgenerate


  assign apbsubsys_interrupt[31:0] = {
                   {11{1'b0}},                       // 21-31 (AHB GPIO #0 individual interrupt)
                   i_i2c1_int_tx,                    // 20
                   i_i2c1_int_rx,                    // 19
                   i_i2c0_int_tx,                    // 18
                   i_i2c0_int_rx,                    // 17
                   i_ssp1_interrupt,                 // 16
                   i_ssp0_interrupt,                 // 15
                   i_dualtimer0_int,                 // 14
                   i_timer1_int,                     // 13
                   i_timer0_int,                     // 12         
                   i_uart3_overflow_int,             // 11
                   i_uart2_overflow_int,             // 10
                   i_uart1_overflow_int,             // 9
                   i_uart0_overflow_int,             // 8                   
                   i_uart3_txint,                    // 7
                   i_uart3_rxint,                    // 6
                   i_uart2_txint,                    // 5
                   i_uart2_rxint,                    // 4
                   i_uart1_txint,                    // 3
                   i_uart1_rxint,                    // 2
                   i_uart0_txint,                    // 1
                   i_uart0_rxint};                   // 0

  assign watchdog_interrupt = i_watchdog_int;
  assign watchdog_reset     = i_watchdog_rst;

 `ifdef ARM_APB_ASSERT_ON
   // ------------------------------------------------------------
   // Assertions
   // ------------------------------------------------------------
`include "std_ovl_defines.h"

   // PSEL should be one-hot
   // If this OVL fires - there is an error in the design of the address decoder
   assert_zero_one_hot
     #(`OVL_FATAL,16,`OVL_ASSERT,
       "Only one PSEL input can be activated.")
   u_ovl_psel_one_hot
     (.clk(PCLK), .reset_n(PRESETn),
      .test_expr({timer0_psel,timer1_psel,dualtimer0_psel,uart0_psel,uart1_psel,uart2_psel,uart3_psel,watchdog_psel,
                  ssp0_psel,ssp1_psel,i2c0_psel,i2c1_psel,ext12_psel,ext13_psel,ext14_psel,ext15_psel,ssp0_psel,ssp1_psel}));


   // All Writes to the APB peripherals must be word size since PSTRB only
   // supported on the APB test slave. Therefore, the AHB bridge can generate
   // non-word sized writes which can break the APB peripherals (not
   // including the test slave) since they don't support this (i.e. PSTRB
   // is not present). Hence, restrict the appropriate accesses to word sized
   // writes.
   assert_implication
     #(`OVL_ERROR,`OVL_ASSERT,
       "All Writes to the APB peripherals must be word size not including the test slave")
   u_ovl_apb_write_word_size_32bits
     (.clk              (PCLK),
      .reset_n          (PRESETn),
      .antecedent_expr  (i_penable && i_psel && i_pwrite && (~test_slave_psel)),
      .consequent_expr  (i_pstrb == 4'b1111)
      );

  // This protocol checker is placed here and attached to the PCLK and PCLKEN.
  // A note should be made that this means that the value of PCLKEN may not
  // necessarily be the same as the enable term that is gating PCLK to generate
  // PCLKG.
  ApbPC  #(.ADDR_WIDTH                   (16),
           .DATA_WIDTH                   (32),
           .SEL_WIDTH                    (1),
           // OVL instances property_type (0=assert, 1=assume, 2=ignore)
           .MASTER_REQUIREMENT_PROPTYPE  (0),
           .SLAVE_REQUIREMENT_PROPTYPE   (0),

           .PREADY_FUNCTIONAL            (1),
           .PSLVERR_FUNCTIONAL           (1),
           .PPROT_FUNCTIONAL             (1),
           .PSTRB_FUNCTIONAL             (1)
           ) u_ApbPC
    (
     // Inputs
     .PCLK       (PCLK),
     .PRESETn    (PRESETn),
     .PSELx      (i_psel),
     .PPROT      (i_pprot),
     .PSTRB      (i_pstrb),
     .PENABLE    (i_penable),
     .PREADY     (i_pready_mux),
     .PSLVERR    (i_pslverr_mux),
     .PADDR      (i_paddr),
     .PWRITE     (i_pwrite),
     .PWDATA     (i_pwdata),
     .PRDATA     (i_prdata_mux)
     );


`else
    // Lint checker
    wire unused = (|i_pprot);                        // used in assertions only

`endif

endmodule
