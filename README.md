# Carrot Core

#### 介绍
基于EG4S20BG256编写的ARM M0内核完整电路代码，但需要自己修改cmsdk_mcu_pin_mux.v中的代码实现引脚复用，目前实现了定时器、中断、I2C、Uart、SPI、GPIO等功能

#### 软件架构
使用安路官方TD5.0.5开发环境

#### 使用说明
1.  使用TD5.0.5打开文件中的Carrot-M0.al文件
2.  需要自己修改部分逻辑，此工程剩余一小部分功能未完善
3.  已提供GPIO和定时器以及UART控制的keil工程

#### 构建成员
FYX-TonXent WT HT
